<?php

namespace App\Http\Controllers;

use App\Models\customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['customer']=DB::table('customers')->get();
        $data['title']='Customers ';
         $data['routes'] = "customer";
        $data['pagetitle'] = 'Customer | Laravel8 Demo'; 
        return view('admin.Customer.customer',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']='Customers ';
        $data['subtitle'] ='Create Customer';
        $data['routes'] = "customer";
        $data['pagetitle'] = 'Customer | Laravel8 Demo'; 
        return view('admin.Customer.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required', 
            'email' => 'unique:customers|max:255',
            'phoneNo' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:customers',
            'address'=>'required'
        ]);
    
        $data = new User;
        $data= $request->all();
        // echo"<pre>"; print_r($data); exit();
        $customer=Customer::create($data);
        if($customer)
        {
            request()->session()->flash('success','Customer created successfully.');
        }
        else {
            request()->session()->flash('error','Something Went Wrong Please Try Again.');
        }
        return redirect('customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$id) 
    {
        // echo"<pre>"; print_r($request->all()); exit();

        $data['customer']=Customer::find($id);
        $data['title']='Customers ';
        $data['subtitle'] ='Edit Customer';
        $data['routes'] = "customer";
        $data['pagetitle'] = 'Customer | Laravel8 Demo'; 
        return view('admin.Customer.edit',$data);         }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $UpdateCustomer=Customer::find($id);
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:customers,email,'.$id, 
            'phoneNo' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:customers,phoneNo,'.$id,
            'address'=>'required'
        ]);
    
        $data= $request->all();
        // echo"<pre>"; print_r($data); exit();
        $customer=$UpdateCustomer->fill($data)->save();
        if($customer)
        {
            request()->session()->flash('success','Customer Updated successfully.');
        }
        else {
            request()->session()->flash('error','Something Went Wrong Please Try Again.');
        }
        return redirect('customer');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer=Customer::find($id);
        // echo"<pre>"; print_r($customer); exit();
        
        if($customer){
            $status=$customer->delete();
             if($status)
        {
            request()->session()->flash('success','Customer Deleted successfully.');
        }
        else {
            request()->session()->flash('error','Something Went Wrong Please Try Again.');
        }
        return redirect('customer');
        }
    }
}
