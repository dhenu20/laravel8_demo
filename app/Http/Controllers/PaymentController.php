<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Stripe;
use Razorpay\Api\Api;
use Exception;
// use Srmklive\PayPal\Facades\PayPal;
use Srmklive\PayPal\Services\ExpressCheckout;


class PaymentController extends Controller
{
    public function __construct()
    {
        $this->provider = new ExpressCheckout();
    }
    public function stripe()
    {
        $data['title']='Stripe ';
        $data['subtitle'] ='Stripe Payment';
        $data['routes'] = "stripe";
        $data['pagetitle'] = 'Stripe | Laravel8 Demo'; 
        return view('admin.Payment.stripe',$data);
    }
   
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 100 * 100,
                "currency" => "INR",
                "source" => $request->stripeToken,
                "description" => "This payment is tested purpose only"
        ]);
   
        Session::flash('success', 'Payment successful!');
           
        return back();
    }

     public function razorpay()
    {        
        $data['title']='RazorPay ';
        $data['subtitle'] ='RazorPay Payment';
        $data['routes'] = "razorpay";
        $data['pagetitle'] = 'RazorPay | Laravel8 Demo'; 
        return view('admin.Payment.razorpay',$data);
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function razorpayPost(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); exit();
        $input = $request->all();
  
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
  
        $payment = $api->payment->fetch($input['razorpay_payment_id']);
  
        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 
  
            } catch (Exception $e) {
                return  $e->getMessage();
                Session::put('error',$e->getMessage());
                return redirect()->back();
            }
        }
          
        Session::put('success', 'Payment successful');
        return redirect()->back();
    }

    /**
     * * Paypal Functionality
     */
   
     public function paypal()
    {
        $data = [];
        $data['items'] = [
            [
                'name' => 'Laravel8 Demo',
                'price' => 100,
                'desc'  => 'Description here',
                'qty' => 1
            ]
        ];
  
        $data['invoice_id'] = 1;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('paypal.success');
        $data['cancel_url'] = route('paypal.cancel');
        $data['total'] = 100;
  
        $provider = $this->provider;;
  
        $response = $provider->setExpressCheckout($data);
  
        $response = $provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        dd('Your payment is canceled. You can create cancel page here.');
    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function paypalPost(Request $request)
    {
      
        // echo "<pre>"; print_r($request->all()); exit();

        $provider = $this->provider;
        $response = $provider->getExpressCheckoutDetails($request->token);
  
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
        
         request()->session()->flash('success','Payment was successfull. The payment success page goes here!.');
 
        }
        else{
        dd('Error occured!');
        }
        return redirect('p');
  
    }

    
   
}
