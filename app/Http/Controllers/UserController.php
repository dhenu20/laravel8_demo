<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user']=DB::table('users')->get();
        $data['title']='User ';
        $data['routes'] = "user";
        $data['pagetitle'] = 'User | Laravel8 Demo'; 
        return view('admin.User.user',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['subtitle'] ='Create User';
        $data['title']='User ';
        $data['routes'] = "user";
        $data['pagetitle'] = 'User | Laravel8 Demo'; 
        return view('admin.User.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // echo"<pre>"; print_r($request->all()); exit();
        $this->validate($request,[
             'name' => 'required',
            'email' => 'required',
            'email' => 'unique:users|max:255',
            'password' => 'required|min:8',
           ]);
    
        

        $res = new User;
        $res->name = ucwords($request->input('name'));
        $res->email = trim($request->input('email'));
        $res->password = Hash::make($request->input('password'));
        // echo "<pre>"; print_r($res); exit();
        $res->save();
       
        if($res)
        {
            request()->session()->flash('success','User created successfully.');
        }
        else {
            request()->session()->flash('error','Something Went Wrong Please Try Again.');
        }
        return redirect('suser.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result['title'] = 'User List';
        $result['subtitle'] = 'Edit User';
        $result['routes'] = "users";
        $result['pagetitle'] = 'Edit User | Laravel8 Demo';
        $result['editdata'] = User::find($id);
        return view('admin/user/edit',$result);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo "<pre>"; print_r($request->all()); exit();
            
        //     $this->validate($request,[
        //     'name' => 'required',
        //     'email' => 'required|email|unique:users,email,'.$id,

        //    ]);
    
       
        $res =User::find($id);
        $res->name = ucwords($request->input('name'));
        $res->email = trim($request->input('email'));
        
        if(isset($password) && $password != ''){
        $res->password = Hash::make($request->input('password'));
     }
        $res->save();
       
        if($res)
        {
            request()->session()->flash('success','User Updated successfully.');
        }
        else {
            request()->session()->flash('error','Something Went Wrong Please Try Again.');
        }
        return redirect('user');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::findorFail($id);

       $status=$user->delete();
       
        if($status){
            request()->session()->flash('success','User deleted successfully.');
        }
        else{
            request()->session()->flash('error','Error while deleting user');
        }
        
        return redirect('user.index');
    }
}
