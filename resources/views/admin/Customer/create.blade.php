@include('admin.layouts.stylesheet')
<!-- Navbar -->
@include('admin.layouts.navbar')
<!-- /.navbar -->
<!-- Main Sidebar Container -->
@include('admin.layouts.mainsidebar')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">{{$subtitle}}</h1>
            </div>
            <!-- /.col -->
            <!-- Content Wrapper. Contains page content -->
            @include('admin.layouts.breadcrumb')
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <section class="content">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <!-- /.card-header -->
               <div class="card-body">
                  <h3 class="card-title float-right">
                     <a title="Back" href="{{route('customer')}}"><button title="Back" type="button" class="btn btn-block btn-primary">Back</button></a>
                  </h3>
                  <br>
                  <div class="col-xl-10 col-lg-6 col-md-8 col-sm-10 mx-auto">
                     <br>
                     
                     <!-- /.card-header -->
                     <!-- form start -->
                     <div class="card card-primary ">
                        <div class="card-header">
                           <h3 class="card-title">Create Customer</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal" action="{{route('StoreCustomer')}}" method="post" enctype="multipart/form-data">
                           @csrf
                           <div class="card-body">
                              <div class="form-group">
                                 <label for="name">Name <span class="mendatory">*</span></label>
                                 <input class="form-control" id="nametext"  type="text" name="name" placeholder="Enter name" value="{{old('name')}}"  autocomplete="name" required>
                                 @error('name')
                                 <span class="text-danger">{{$message}}</span>
                                 @enderror
                              </div>
                              <div class="form-group">
                                 <label for="email">Email  <span class="mendatory">*</span></label>
                                 <input class="form-control" id="email" value="{{old('email')}}"   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" type="email" name="email" placeholder="Enter email" autocomplete="email" required>
                                 @error('email')
                                 <span class="text-danger">{{$message}}</span>
                                 @enderror
                              </div>
                            
                              <div class="form-group">  
                                 <label for="exampleInputPassword1">Mobile no<span class="mendatory">*</span></label>
                                 <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                           <span class="input-group-text">+91</span>
                                             </div>
                                 <input class="form-control"   onkeypress="return (event.charCode !=8 && event.charCode ==0 || ( event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)))" id="phoneNo" type="text" name="phoneNo" placeholder="Enter 10 digit number" maxlength="10" pattern=".{5}|.{10,10}" value="{{old('phoneNo')}}"   autocomplete="phoneNo" > </div>
                                 @error('phoneNo')
                                 <span class="text-danger">{{$message}}</span>
                                 @enderror
                              </div>

                              <div class="form-group">
                                 <label for="exampleInputPassword1">Address<span class="mendatory">*</span></label>
                                 <textarea type="text"  class="form-control" id="address"   name="address"placeholder="Address" id="address"> {{old('address')}} </textarea>
                                 @error('address')
                                 <span class="text-danger">{{$message}}</span>
                                 @enderror
                              </div>

                           <!-- /.card-body -->
                           <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                              <button type="reset" class="btn btn-warning">Reset</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('admin.layouts.footer')
<!-- ./wrapper -->
<!-- jQuery -->
@include('admin.layouts.scripts')