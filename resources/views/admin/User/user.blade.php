  @include('admin.layouts.stylesheet')
<!-- Navbar -->
@include('admin.layouts.navbar')
<!-- /.navbar -->
<!-- Main Sidebar Container -->
@include('admin.layouts.mainsidebar')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">{{$title}}</h1>
            </div>
            <!-- /.col -->
            <!-- Content Wrapper. Contains page content -->
            @include('admin.layouts.breadcrumb')
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-12">
            <div class="row">
               <div class="col-md-12">
                  @include('admin.layouts.notification')
               </div>
            </div>
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title float-right"> <a title="Create User" href="{{route('user.create')}}">
                     <button type="
                        button" title="Create User" class="btn btn-block btn-primary"> Create User</button></a>
                  </h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <label id="convertmsg" class="col-md-8  alert-success"></label>
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                            <th>Id</th>
                           <th>Name</th>
                           <th>Email</th>
                          <!--  <th>Role</th> -->
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($user as $users)
                      
                        <tr>
                           <td class="text-center">{{$users->id}}</td>
                           <td class="text-center">{{$users->name}}</td>
                           <td class="text-center">{{$users->email}}</td>
                           <td>
                           
                   <a class="btn btn-primary btn-sm " style="height:30px; width:30px;border-radius:50%"  title="Edit" href="{{route('user.edit',$users->id)}}"><i class="fa fa-edit"></i></a>
                <form action="{{ route('DeleteCustomer',$users->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger btn-sm " style="height:30px; width:30px;border-radius:50%"  title="Delete" href="{{route('user.destroy',$users->id)}}" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i></button>
                 
                </form>
            </td>
            @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('admin.layouts.footer')
<!-- ./wrapper -->
<!-- jQuery -->
@include('admin.layouts.scripts')
<style type="text/css">
   button.btn.active.btn-Active.btn-md {
   font-size: 10px;
   padding: 4px;
   background-color: green !important;
   }
</style>
