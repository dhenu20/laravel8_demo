<aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="dashboard" class="brand-link">
  <img src="{{asset('uploads/logo.png')}}" alt=" Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> 
   <span class="brand-text font-weight-light text-center">Laravel Practical</span>
   </a>
   <?php 
  $currentPath= Route::getFacadeRoot()->current()->uri();
  $root = explode('/',$currentPath);
  $root = $root[0];
 ?>
   <!-- Sidebar -->
   <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview menu-open">
               <a href="dashboard" class="nav-link  <?php if($root == 'dashboard'){ echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard</p>
               </a>
            </li>
            <li class="nav-item">
               <a href="user" class="nav-link  <?php if($root == 'user' || $root == 'user.create' || $root == 'user.store' || $root == 'user.edit' || $root == 'user.update' ||  $root == 'user.destroy'  ){ echo 'active'; } ?>">
                  <i class="far fa-user nav-icon"></i>
                  <p>User</p>
               </a>
            </li>
            <li class="nav-item">
               <a href="customer" class="nav-link  <?php if($root == 'customer' || $root == 'CreateCustomer' || $root == 'StoreCustomer' || $root == 'EditCustomer' || $root == 'UpdateCustomer' ||  $root == 'DeleteCustomer'  ){ echo 'active'; } ?>">
                  <i class="fas fa-user-tag nav-icon"></i>
                  <p>Customer</p>
               </a>
            </li>
              <li class="nav-item">
               <a href="stripe" class="nav-link  <?php if($root == 'stripe' || $root == 'stripe.post'){ echo 'active'; } ?>">
                  <i class="fab fa-stripe nav-icon"></i>
                  <p>Stripe Payment</p>
               </a>
            </li>
            <li class="nav-item">
               <a href="razorpay" class="nav-link  <?php if($root == 'razorpay' || $root == 'razorpay.post'){ echo 'active'; } ?>">
                <i class="fab fa-cc-mastercard nav-icon"></i>
                  <p>RazorPay Payment</p>
               </a>
            </li>
            <li class="nav-item">
               <a href="paypal" class="nav-link  <?php if($root == 'paypal' || $root == 'paypal.success'){ echo 'active'; } ?>">
                <i class="fab fa-paypal nav-icon"></i>
                  <p>PayPal Payment</p>
               </a>
            </li>
            <!-- <li class="nav-header pt-1">EXAMPLES</li> -->
           
     <!--  // $role = Session::get('role'); -->
     </ul>
      </nav>
      <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
</aside>
