<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="{{asset('/images/logo.jpg')}}" type="image/x-icon">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>{{$pagetitle}}</title>
<!-- Tell the browser to be responsive to screen width -->

<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('custom/plugins/fontawesome-free/css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{{asset('custom/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
<!-- iCheck {{asset('custom/')}}-->
<link rel="stylesheet" href="{{asset('custom/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- JQVMap -->
<link rel="stylesheet" href="{{asset('custom/plugins/jqvmap/jqvmap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('custom/dist/css/adminlte.min.css')}}">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{asset('custom/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('custom/plugins/daterangepicker/daterangepicker.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('custom/plugins/summernote/summernote-bs4.css')}}">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<!-- DataTables -->
  <link rel="stylesheet" href="{{asset('custom/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  
  <link rel="stylesheet" href="{{asset('custom/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

 <link rel="stylesheet" type="text/css" href="{{asset('custom/dropzone/dist/min/dropzone.min.css')}}">
 

 <!-- image pop -->
 <link rel="stylesheet" href="{{asset('custom/admin/css/image.css')}}">

 				
<style >

	
.mt-3, .my-3 {
    margin-top: 0px !important;
}

</style>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-1"></script>
      <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

      
</head>
<body class="hold-transition sidebar-mini layout-fixed">