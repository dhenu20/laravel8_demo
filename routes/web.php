<?php

use Illuminate\Support\Facades\Route;
// use Laravel\Socialite\Facades\Socialite;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
// Route::get('dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard')->middleware('is_admin');
// Route::get('/', [App\Http\Controllers\HomeController::class, 'adminHome']);

Auth::routes();

//Logout
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class ,'loggedOut'])->name('logout');


//SocialMedia Login
//Google
Route::get('login/google', [App\Http\Controllers\Auth\LoginController::class ,'redirectToProvider'])->name('login/google');
Route::get('login/google/callback', [App\Http\Controllers\Auth\LoginController::class,'handleProviderCallback'])->name('login/google/callback');

//Facebook
Route::get('login/facebook', [App\Http\Controllers\Auth\LoginController::class ,'redirectToFacebook'])->name('login/facebook');
Route::get('login/facebook/callback', [App\Http\Controllers\Auth\LoginController::class,'handleFacebookCallback'])->name('login/facebook/callback');

//Linkedin
Route::get('login/linkedin', [App\Http\Controllers\Auth\LoginController::class ,'redirectToLinkedin'])->name('login/linkedin');
Route::get('login/linkedin/callback', [App\Http\Controllers\Auth\LoginController::class,'handleLinkedinCallback'])->name('login/linkedin/callback');

//Twitter
Route::get('login/twitter', [App\Http\Controllers\Auth\LoginController::class ,'redirectToTwitter'])->name('login/twitter');
Route::get('login/twitter/callback', [App\Http\Controllers\Auth\LoginController::class,'handleTwitterCallback'])->name('login/twitter/callback');

//Github
Route::get('login/github', [App\Http\Controllers\Auth\LoginController::class ,'redirectToGithub'])->name('login/github');
Route::get('login/github/callback', [App\Http\Controllers\Auth\LoginController::class,'handleGithubCallback'])->name('login/github/callback');

//Customer
Route::get('/customer', [App\Http\Controllers\CustomerController::class, 'index'])->name('customer');
Route::get('/create', [App\Http\Controllers\CustomerController::class, 'create'])->name('CreateCustomer');
Route::post('/store', [App\Http\Controllers\CustomerController::class, 'store'])->name('StoreCustomer');
Route::get('/edit/{id}', [App\Http\Controllers\CustomerController::class, 'edit'])->name('EditCustomer');
Route::post('/update/{id}', [App\Http\Controllers\CustomerController::class, 'update'])->name('UpdateCustomer');
Route::delete('/delete/{id}', [App\Http\Controllers\CustomerController::class, 'destroy'])->name('DeleteCustomer');

//User
Route::resource('/user', App\Http\Controllers\UserController::class);

//Stripe Payment
Route::get('stripe', [App\Http\Controllers\PaymentController::class, 'stripe']);
Route::post('stripe', [App\Http\Controllers\PaymentController::class, 'stripePost'])->name('stripe.post');


//Rozorpay Payment
Route::get('razorpay', [App\Http\Controllers\PaymentController::class, 'razorpay']);
Route::post('razorpay', [App\Http\Controllers\PaymentController::class, 'razorpayPost'])->name('razorpay.post');

//PayPal Payment
Route::get('paypal', [App\Http\Controllers\PaymentController::class, 'paypal']);
Route::get('cancel', [App\Http\Controllers\PaymentController::class, 'cancel'])->name('paypal.cancel');
Route::get('paypal/paypalPost', [App\Http\Controllers\PaymentController::class, 'paypalPost'])->name('paypal.success');
